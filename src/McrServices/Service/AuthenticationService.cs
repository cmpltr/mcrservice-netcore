using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Security.Authentication;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using McrService.NET.Models;
using McrService.NET.Security;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace McrService.NET.Service {
    public interface IAuthenticationService {
        Task<IPrincipal> Authenticate(string username, string password);
    }

    public class AuthenticationService : IAuthenticationService {
        private readonly ILogger<AuthenticationService> _log;

        // private readonly UserManager<User> _userManager;
        private readonly IUsuarioService _userManager;

        // public AuthenticationService(ILogger<AuthenticationService> log, UserManager<User> userManager)
        public AuthenticationService(ILogger<AuthenticationService> log, IUsuarioService userManager)
        {
            _log = log;
            _userManager = userManager;
        }

        public async Task<IPrincipal> Authenticate(string username, string password)
        {
            //https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.identity.signinmanager-1.passwordsigninasync?view=aspnetcore-2.2
            // => not use because cookie
            //=> https://stackoverflow.com/questions/53854051/usermanager-checkpasswordasync-vs-signinmanager-passwordsigninasync
            //https://github.com/openiddict/openiddict-core/issues/578

            var user = await LoadUserByUsername(username);

            if (!user.Activated) throw new UserNotActivatedException($"User {user.UserName} was not activated.");

            bool checkedPassword = user.PasswordHash.Equals(password);
            // if (await _userManager.CheckPasswordAsync(user, password)) return await CreatePrincipal(user);
            if (checkedPassword) return await CreatePrincipal(user);

            _log.LogDebug("Authentication failed: password does not match stored value");
            throw new InvalidCredentialException("Authentication failed: password does not match stored value");
        }

        private async Task<User> LoadUserByUsername(string username)
        {
            _log.LogDebug($"Authenticating {username}");

            // if (new EmailAddressAttribute().IsValid(username)) {
            //     var userByEmail = await _userManager.FindByEmailAsync(username);
            //     if (userByEmail == null)
            //         throw new UsernameNotFoundException(
            //             $"User with email {username} was not found in the database");
            //     return userByEmail;
            // }

            var lowerCaseLogin = username.ToLower(CultureInfo.GetCultureInfo("en-US"));
            var userDB = await _userManager.FindByLogin(username);

            if (userDB == null)
                throw new UsernameNotFoundException($"User {lowerCaseLogin} was not found in the database");
            var userByLogin = 
                new User {
                    Id = userDB.Usuarioid.ToString(),
                    UserName = userDB.Cpf,
                    PasswordHash = userDB.Senha,
                    Activated = true,
                    LangKey = "pt-BR"
                };
            return userByLogin;
        }

        private async Task<IPrincipal> CreatePrincipal(User user)
        {
            var claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.UserName)
            };
            // var roles = await _userManager.GetRolesAsync(user);
            var roles = await UserRoles();
            claims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));
            var identity = new ClaimsIdentity(claims);
            return new ClaimsPrincipal(identity);
        }

        private static Task<IList<string>> UserRoles()
        {
            IList<string> listRoles = new List<string>();
            listRoles.Add("ROLE_USER");
            return Task.FromResult<IList<string>>(listRoles);
        }
    }
}
