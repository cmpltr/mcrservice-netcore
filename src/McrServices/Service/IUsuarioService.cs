using McrService.NET.Models;
using System.Linq;
using System.Threading.Tasks;

namespace McrService.NET.Service
{
    public interface IUsuarioService
    {
        Usuario RecuperaUsuarioValido(Usuario usuario);
        Task<Usuario> FindByLogin(string username);
        bool CheckPasswordAsync(Usuario user, string password);

        IQueryable<Usuario> GetAll();
    }
}