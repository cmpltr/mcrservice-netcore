using AutoMapper;
using McrService.NET.Models;
using McrService.NET.Service.Dto;

namespace McrService.NET.Service.Mapper {
    public class UserProfile : Profile {
        public UserProfile()
        {
            CreateMap<User, UserDto>();
        }
    }
}
