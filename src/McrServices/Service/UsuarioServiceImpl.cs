using System.Linq;
using System;
using System.Threading.Tasks;
using McrService.NET.Infrastructure.Repositories;
using McrService.NET.Models;
using Microsoft.Extensions.Logging;

namespace McrService.NET.Service
{
    public class UsuarioServiceImpl : IUsuarioService
    {
        private readonly ILogger<UsuarioServiceImpl> _log;
        private readonly IUsuarioRepository _usuarioRepository;
        public UsuarioServiceImpl(ILogger<UsuarioServiceImpl> log, IUsuarioRepository usuarioRepository) 
        {
            this._log = log;
            this._usuarioRepository = usuarioRepository;
        }

        public bool CheckPasswordAsync(Usuario user, string password)
        {
            return user.Senha.Equals(password);
        }

        public Task<Usuario> FindByLogin(string login)
        {
            try {
                var usuario = _usuarioRepository.FindByLogin(login);
                return Task.FromResult<Usuario> (usuario);
            } catch (Exception e) {
                _log.LogError(e, "Usuário não encontrado pelo login ${login}", login);
                throw e;
            }
        }

        public IQueryable<Usuario> GetAll() {
            try {
                return _usuarioRepository.GetAll();
            } catch (Exception e) {
                _log.LogError(e, "Erro na pesquisa de todos usuario");
                throw e;
            }
        }

        public Usuario RecuperaUsuarioValido(Usuario usuario)
        {
            throw new System.NotImplementedException();
        }
    }
}