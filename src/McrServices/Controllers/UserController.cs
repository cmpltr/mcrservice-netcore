using System.Security.Claims;
using System.Collections.Generic;
using System.Threading.Tasks;
using JHipsterNet.Pagination;
using JHipsterNet.Pagination.Extensions;
using McrService.NET.Models;
using McrService.NET.Security;
using McrService.NET.Service;
using McrService.NET.Service.Dto;
using McrService.NET.Web.Extensions;
using McrService.NET.Web.Filters;
using McrService.NET.Web.Rest.Problems;
using McrService.NET.Web.Rest.Utilities;
using McrServices.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace McrService.NET.Controllers {
    [Route("api")]
    [ApiController]
    public class UserController : ControllerBase {
        private readonly ILogger<UserJwtController> _log;
        private readonly IMailService _mailService;
        // private readonly UserManager<User> _userManager;
        private readonly IUsuarioService _userManager;
        // private readonly IUserService _userService;

        private readonly ITokenClaimedService _tokenClaimedService;

        public UserController(ILogger<UserJwtController> log, IUsuarioService userManager,
            IMailService mailService, ITokenClaimedService tokenClaimedService)
        {
            _log = log;
            _userManager = userManager;
            // _userService = userService;
            _mailService = mailService;
            _tokenClaimedService = tokenClaimedService;
        }

        // [HttpPost("users")]
        // [ValidateModel]
        // public async Task<ActionResult<User>> CreateUser([FromBody] UserDto userDto)
        // {
        //     _log.LogDebug($"REST request to save User : {userDto}");
        //     if (userDto.Id != null)
        //         throw new BadRequestAlertException("A new user cannot already have an ID", "userManagement",
        //             "idexists");
        //     // Lowercase the user login before comparing with database
        //     if (await _userManager.FindByNameAsync(userDto.Login.ToLowerInvariant()) != null)
        //         throw new LoginAlreadyUsedException();
        //     if (await _userManager.FindByEmailAsync(userDto.Email.ToLowerInvariant()) != null)
        //         throw new EmailAlreadyUsedException();

        //     var newUser = await _userService.CreateUser(userDto);
        //     await _mailService.SendCreationEmail(newUser);
        //     return CreatedAtAction(nameof(GetUser), new {login = newUser.Login}, newUser)
        //         .WithHeaders(HeaderUtil.CreateEntityCreationAlert("userManagement.created", newUser.Login));
        // }

        // [HttpPut("users")]
        // [ValidateModel]
        // public async Task<IActionResult> UpdateUser([FromBody] UserDto userDto)
        // {
        //     _log.LogDebug($"REST request to update User : {userDto}");
        //     var existingUser = await _userManager.FindByEmailAsync(userDto.Email);
        //     if (existingUser != null && !existingUser.Id.Equals(userDto.Id)) throw new EmailAlreadyUsedException();
        //     existingUser = await _userManager.FindByNameAsync(userDto.Login);
        //     if (existingUser != null && !existingUser.Id.Equals(userDto.Id)) throw new LoginAlreadyUsedException();

        //     var updatedUser = await _userService.UpdateUser(userDto);

        //     return ActionResultUtil.WrapOrNotFound(updatedUser)
        //         .WithHeaders(HeaderUtil.CreateAlert("userManagement.updated", userDto.Login));
        // }

        [HttpGet("users")]
        [Authorize]
        public ActionResult<IEnumerable<User>> GetAllUsers(IPageable pageable)
        {
            _log.LogDebug("REST request to get a page of Users");
            _log.LogDebug("######### USUARIO LOGADO ["+_tokenClaimedService.UserNameClaimed() +"]");
            string value = this.User.Identity.Name;
            _log.LogDebug("######### USUARIO LOGADO ["+value +"]");
            var page = _userManager.GetAll().UsePageable(pageable);
            var headers = PaginationUtil.GeneratePaginationHttpHeaders(page, HttpContext.Request);
            return Ok(page.Content).WithHeaders(headers);
        }

        // [HttpGet("users/authorities")]
        // [Authorize]
        // public ActionResult<IEnumerable<string>> GetAuthorities()
        // {
        //     return Ok(_userService.GetAuthorities());
        // }

        [HttpGet("users/{login}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetUser([FromRoute] string login)
        {
            _log.LogDebug($"REST request to get User : {login}");
            var result = await _userManager.FindByLogin(login);
            //TODO adopt a more idiomatic syntax with LINQ Select/Where cf. cancelation token
            var userDto = result != null ? result : null;
            return ActionResultUtil.WrapOrNotFound(userDto);
        }

        // [HttpDelete("users/{login}")]
        // [Authorize]
        // public async Task<IActionResult> DeleteUser([FromRoute] string login)
        // {
        //     _log.LogDebug($"REST request to delete User : {login}");
        //     await _userService.DeleteUser(login);
        //     return Ok().WithHeaders(HeaderUtil.CreateEntityDeletionAlert("userManagement.deleted", login));
        // }
    }
}
