using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.IO;
using JHipsterNet.Logging;
using ILogger = Serilog.ILogger;

using Microsoft.Extensions.Logging;
using Steeltoe.Extensions.Logging;
using Steeltoe.Extensions.Configuration.CloudFoundry;
using Steeltoe.Extensions.Configuration.PlaceholderCore;
using Steeltoe.Extensions.Configuration.RandomValue;

using static JHipsterNet.Boot.BannerPrinter;

namespace McrService.NET {
    public class Program {

        public static int Main(string[] args)
        {
            PrintBanner(10 * 1000);

            try {

                Log.Logger = CreateLogger();

                IWebHost webHost = CreateWebHostBuilder(args).Build();

                webHost.Run();

                return 0;

            }
            catch (Exception ex) {
                // Use ForContext to give a context to this static environment (for Serilog LoggerNameEnricher).
                Log.ForContext<Program>().Fatal(ex, $"Host terminated unexpectedly");
                return 1;

            }
            finally {

                Log.CloseAndFlush();

            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(params string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                // .UseCloudFoundryHosting(5051)
                .AddCloudFoundry()
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    config.SetBasePath(builderContext.HostingEnvironment.ContentRootPath)
                        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{builderContext.HostingEnvironment.EnvironmentName}.json", optional: true)
                        // Add RandomValue generator
                        .AddRandomValueSource()
                        .AddEnvironmentVariables();
                })
                .ConfigureLogging((builderContext, loggingBuilder) =>
                {
                    loggingBuilder.ClearProviders();
                    loggingBuilder.AddConfiguration(builderContext.Configuration.GetSection("Logging"));

                    // Add Steeltoe dynamic console logger
                    loggingBuilder.AddDynamicConsole();
                })
                .AddCloudFoundry()
                .AddPlaceholderResolver()
                // https://www.pivotaltracker.com/n/projects/1483238/stories/164820597
                .UseDefaultServiceProvider(options => options.ValidateScopes = false)
                .UseCloudFoundryHosting(5051)
                .UseSerilog()
                .UseStartup<Startup>();
        }

        /// <summary>
        /// Create application logger from configuration.
        /// </summary>
        /// <returns></returns>
        private static ILogger CreateLogger()
        {
            var appConfiguration = GetAppConfiguration();

            // for logger configuration
            // https://github.com/serilog/serilog-settings-configuration
            var loggerConfiguration = new Serilog.LoggerConfiguration()
                .Enrich.With<LoggerNameEnricher>()
                .ReadFrom.Configuration(appConfiguration);

            return loggerConfiguration.CreateLogger();
        }

        /// <summary>
        /// Gets the current application configuration
        /// from global and specific appsettings.
        /// </summary>
        /// <returns>Return the application <see cref="IConfiguration"/></returns>
        private static IConfiguration GetAppConfiguration()
        {
            // Actually, before ASP.NET bootstrap, we must rely on environment variable to get environment name
            // https://docs.microsoft.com/fr-fr/aspnet/core/fundamentals/environments?view=aspnetcore-2.2
            // Pay attention to casing for Linux environment. By default it's pascal case.
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{environment}.json", true)
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
