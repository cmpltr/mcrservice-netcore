using System;
using JHipsterNet.Config;
using McrService.NET.Data;
using McrService.NET.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Steeltoe.Common.HealthChecks;
using Steeltoe.Common.Http.Discovery;
using Steeltoe.Discovery.Client;
using Steeltoe.Management.CloudFoundry;
using Steeltoe.Management.Endpoint.Env;
using Steeltoe.Management.Endpoint.Health;
using Steeltoe.Management.Endpoint.Info;
using Steeltoe.Management.Endpoint.Loggers;
using Steeltoe.Management.Endpoint.Mappings;
using Steeltoe.Management.Endpoint.Metrics;
using Steeltoe.Management.Endpoint.Refresh;
using Steeltoe.Management.Endpoint.Trace;
using Steeltoe.Management.Tracing;
using Steeltoe.Management.Exporter.Tracing;
using McrService.NET.Service;
using McrService.NET.Infrastructure.Repositories;
using McrService.NET.Infrastructure.Repositories.Impls;

[assembly: ApiController]

namespace McrService.NET {
    public sealed class Startup {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<DiscoveryHttpMessageHandler>();
            // Configure a HttpClient 
            //Chamada direta para API B, caso queira testar conectividade/problemas no zuul
            //services.AddHttpClient("api-b", c =>
            //{
            //    c.BaseAddress = new Uri("http://api-b");
            //})
            //.AddHttpMessageHandler<DiscoveryHttpMessageHandler>();

            services.AddDiscoveryClient(Configuration);
            // services.AddSingleton<IHealthContributor, EurekaApplicationsHealthContributor>();
            // services.AddSingleton<IHealthCheckHandler, ScopedEurekaHealthCheckHandler>();

            // Add Distributed tracing
            services.AddDistributedTracing(Configuration);

            // Export traces to Zipkin
            services.AddZipkinExporter(Configuration);

            services.AddMvc();

            // Add Steeltoe Management services
            services.AddCloudFoundryActuators(Configuration);

            services.AddHealthActuator(Configuration);
            services.AddInfoActuator(Configuration);
            services.AddLoggersActuator(Configuration);
            services.AddTraceActuator(Configuration);
            services.AddRefreshActuator(Configuration);
            services.AddEnvActuator(Configuration);
            services.AddMappingsActuator(Configuration);
            services.AddMetricsActuator(Configuration);
            
            services
                .AddNhipsterModule(Configuration)
                .AddDatabaseModule(Configuration)
                .AddSecurityModule()
                .AddProblemDetailsModule()
                .AddAutoMapperModule()
                .AddWebModule()
                .AddSwaggerModule()
                .AddMvc().AddJsonOptions(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.Formatting = Formatting.Indented;
                });

            services.AddScoped<IUsuarioService, UsuarioServiceImpl>()
                    .AddScoped<IUsuarioRepository, UsuarioRepositoryImpl>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        // public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider,
        //     ApplicationDatabaseContext context, IOptions<JHipsterSettings> jhipsterSettingsOptions)
        // {
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider,
            DBCheckProContext context, IOptions<JHipsterSettings> jhipsterSettingsOptions)
        {
            var jhipsterSettings = jhipsterSettingsOptions.Value;

            app.UseStaticFiles();

            // Use the Steeltoe Discovery Client service
            app.UseDiscoveryClient();

            app.UseHealthActuator();
            app.UseInfoActuator();
            app.UseLoggersActuator();
            app.UseTraceActuator();
            app.UseRefreshActuator();
            app.UseEnvActuator();
            app.UseMappingsActuator();
            app.UseMetricsActuator();

            // Add Steeltoe Management endpoints into pipeline
            app.UseCloudFoundryActuators();

            app .UseApplicationSecurity(jhipsterSettings)
                .UseApplicationProblemDetails()
                .UseApplicationWeb(env)
                .UseApplicationSwagger()
                .UseApplicationDatabase(serviceProvider, env);
                // .UseApplicationIdentity(serviceProvider);
            app.UseMvc();


        }
    }
}
