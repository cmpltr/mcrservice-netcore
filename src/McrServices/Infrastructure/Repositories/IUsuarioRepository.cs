using System.Linq;
using McrService.NET.Models;

namespace McrService.NET.Infrastructure.Repositories
{
    public interface IUsuarioRepository
    {
        Usuario FindByLogin(string login);

        IQueryable<Usuario> GetAll();
    }
}