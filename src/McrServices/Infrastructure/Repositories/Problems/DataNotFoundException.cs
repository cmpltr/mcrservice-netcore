using System;

namespace McrService.NET.Infrastructure.Repositories.Problems
{
    [Serializable()]
    public class DataNotFoundException: System.Exception
    {
        public DataNotFoundException() : base () {}

        public DataNotFoundException(string message): base(message) {}

       public DataNotFoundException(string message, System.Exception inner): base(message, inner) {}

        public DataNotFoundException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}