using McrService.NET.Data;
using McrService.NET.Models;

using System.Linq;

namespace McrService.NET.Infrastructure.Repositories.Impls
{
    public class UsuarioRepositoryImpl : AbstractRepository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepositoryImpl(DBCheckProContext context) : base(context)
        {
        }

        public Usuario FindByLogin(string login)
        {
            return DbSet.Single(u => u.Cpf == login);
        }

    }
}