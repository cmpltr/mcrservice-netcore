using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace McrService.NET.Infrastructure {
    public static class SwaggerConfiguration {
        public static IServiceCollection AddSwaggerModule(this IServiceCollection @this)
        {
            @this.AddSwaggerGen(c => {
                c.SwaggerDoc("v2", new Info {Title = "mcroservice-netcore API", Version = "0.0.1"});
            });

            return @this;
        }

        public static IApplicationBuilder UseApplicationSwagger(this IApplicationBuilder @this)
        {
            @this.UseSwagger(c =>
            {
                c.RouteTemplate = "{documentName}/api-docs";
            });
            @this.UseSwaggerUI(c => {
                // c.SwaggerEndpoint("/swagger/v2/swagger.json", "mcroservice-netcore API");
                c.SwaggerEndpoint("/v2/api-docs", "mcroservice-netcore API");
            });
            return @this;
        }
    }
}
