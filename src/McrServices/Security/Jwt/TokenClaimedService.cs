using System.Linq;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using McrServices.Security;

namespace McrServices.Security.Jwt
{
    public class TokenClaimedService : ITokenClaimedService
    {
        private ClaimsPrincipal _claimsPrincipal;
        
        public TokenClaimedService() {

        }

        public ClaimsPrincipal ClaimsPrincipal {
            get => this._claimsPrincipal;
            set => this._claimsPrincipal = value;
        }

        public string UserNameClaimed() {
            string v = _claimsPrincipal.Claims.Filter(c => c.Type == JwtRegisteredClaimNames.Sub).First().Value.ToString();
            return v;
        }

    }
}