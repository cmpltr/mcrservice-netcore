using System.Security.Claims;
using System.Threading.Tasks;
using McrServices.Security;
using Microsoft.AspNetCore.Authentication;

namespace McrService.NET.Security.Jwt {
    public class RoleClaimsTransformation : IClaimsTransformation {
        private readonly ITokenProvider _tokenProvider;
        private readonly ITokenClaimedService _tokenClaimedService;

        public RoleClaimsTransformation(ITokenProvider tokenProvider, ITokenClaimedService tokenClaimedService)
        {
            _tokenProvider = tokenProvider;
            _tokenClaimedService = tokenClaimedService;
        }

        public Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            Task<ClaimsPrincipal> claimsPrincipal = Task.FromResult(_tokenProvider.TransformPrincipal(principal));
            this._tokenClaimedService.ClaimsPrincipal = claimsPrincipal.Result;
            return Task.FromResult(_tokenProvider.TransformPrincipal(principal));
        }
    }
}
