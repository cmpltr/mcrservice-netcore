using System.Security.Claims;

namespace McrServices.Security
{
    public interface ITokenClaimedService
    {

         ClaimsPrincipal ClaimsPrincipal { get; set; }
         string UserNameClaimed();
    }
}