﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

using McrService.NET.Models;

namespace McrService.NET.Data
{
    public partial class DBCheckProContext : DbContext
    {
        public DBCheckProContext()
        {
        }

        public DBCheckProContext(DbContextOptions<DBCheckProContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Usuario> Usuario { get; set; }
 

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=192.168.40.30;Port=5432;Pooling=true;Database=db_checkpro;User Id=checkpro;Password=checkpro;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

 

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.ForNpgsqlHasComment("Usuario");

                entity.Property(e => e.Usuarioid).ForNpgsqlHasComment("UsuarioId");

                entity.Property(e => e.Ativo).ForNpgsqlHasComment("Ativo");

                entity.Property(e => e.Consultatipoid)
                    .HasDefaultValueSql("10")
                    .ForNpgsqlHasComment("ConsultaTipoId");

                entity.Property(e => e.Cpf).ForNpgsqlHasComment("CPF");

                entity.Property(e => e.Dataatualizacao).ForNpgsqlHasComment("DataAtualizacao");

                entity.Property(e => e.Datainsercao)
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ForNpgsqlHasComment("DataInsercao");

                entity.Property(e => e.Email).ForNpgsqlHasComment("Email");

                entity.Property(e => e.Empresaid).ForNpgsqlHasComment("EmpresaId");

                entity.Property(e => e.Foto).ForNpgsqlHasComment("Foto");

                entity.Property(e => e.Nome).ForNpgsqlHasComment("Nome");

                entity.Property(e => e.Perfilid).ForNpgsqlHasComment("PerfilId");

                entity.Property(e => e.Saldo).ForNpgsqlHasComment("Saldo");

                entity.Property(e => e.Saldoilimitado).ForNpgsqlHasComment("SaldoIlimitado");

                entity.Property(e => e.Senha).ForNpgsqlHasComment("Senha");

                entity.Property(e => e.Senhasalt).ForNpgsqlHasComment("SenhaSalt");

                entity.Property(e => e.Telefone).ForNpgsqlHasComment("Telefone");

                entity.Property(e => e.Telefone2).ForNpgsqlHasComment("Telefone2");

                entity.Property(e => e.Termouso).ForNpgsqlHasComment("TermoUso");


            });



            modelBuilder.HasSequence("s_usuario");

        }
    }
}
