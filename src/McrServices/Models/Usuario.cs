﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace McrService.NET.Models 
{
    [Table("usuario")]
    public partial class Usuario
    {
        public Usuario()
        {
        }

        [Column("usuarioid")]
        public int Usuarioid { get; set; }
        [Column("empresaid")]
        public int Empresaid { get; set; }
        [Column("perfilid")]
        public int Perfilid { get; set; }
        [Required]
        [Column("cpf")]
        [StringLength(11)]
        public string Cpf { get; set; }
        [Required]
        [Column("nome")]
        [StringLength(200)]
        public string Nome { get; set; }
        [Required]
        [Column("senha", TypeName = "character(16)")]
        public string Senha { get; set; }
        [Required]
        [Column("senhasalt", TypeName = "character(32)")]
        public string Senhasalt { get; set; }
        [Required]
        [Column("email")]
        [StringLength(100)]
        public string Email { get; set; }
        [Required]
        [Column("telefone")]
        [StringLength(15)]
        public string Telefone { get; set; }
        [Column("telefone2")]
        [StringLength(15)]
        public string Telefone2 { get; set; }
        [Column("saldo", TypeName = "numeric(6,2)")]
        public decimal? Saldo { get; set; }
        [Column("saldoilimitado")]
        public bool Saldoilimitado { get; set; }
        [Column("termouso")]
        public bool? Termouso { get; set; }
        [Column("ativo")]
        public bool Ativo { get; set; }
        [Column("dataatualizacao", TypeName = "date")]
        public DateTime? Dataatualizacao { get; set; }
        [Column("datainsercao")]
        public DateTime Datainsercao { get; set; }
        [Column("foto")]
        public byte[] Foto { get; set; }
        [Column("consultatipoid")]
        public int Consultatipoid { get; set; }

    }
}
