# McrServices

This application was generated using JHipster and JHipster .Net Core 0.1.0, you can find documentation and help at [https://www.jhipster.tech/documentation-archive/vundefined](https://www.jhipster.tech/documentation-archive/vundefined).

## Development

To start your application in the Debug configuration, simply run:

    dotnet run --verbosity normal --project ./src/McrServices/McrServices.csproj

For further instructions on how to develop with JHipster, have a look at [Using JHipster in development][].

## dockfile

Argument to environment 
- ASPNETCORE_ENVIROMENT=Production

## Building for production

To build the arifacts and optimize the McrServices application for production, run:

    cd ./src/McrServices
    rm -rf ./src/McrServices/wwwroot
    dotnet publish --verbosity normal -c Release -o ./app/out ./McrServices.csproj

The `./src/McrServices/app/out` directory will contain your application dll and its depedencies.

## Testing

To launch your application's tests, run:

    dotnet test --list-tests --verbosity normal

### Code quality

// TODO

## Using Docker to simplify development (optional)

// TODO

## Build a Docker image

You can also fully dockerize your application and all the services that it depends on. To achieve this, first build a docker image of your app by running:

    docker build -f ./src/McrServices/Dockerfile -t McrServices .

Then run:

    docker run -p 80:80 McrServices

## Continuous Integration (optional)

// TODO
